class SerialForm extends React.Component {
  constructor () {
    super()
    this.handleSubmit = this.handleSubmit.bind(this)
    this.setValue = this.setValue.bind(this)
  }
  handleSubmit (e) {
    e.preventDefault()
    this.props.onSerialSubmit(this.state)
  }
  setValue (e, key) {
    let obj = {}
    obj[key] = e.target.value
    this.setState(obj)
  }
  render() {
    return (
      <form className='col s12' onSubmit={this.handleSubmit}>
        <InputField
          component={{class:Textbox, name:'Serial Code (optional)', id:'serialcode'}}
          onChangeValue={this.setValue}/>
        <InputField
          component={{class:Textbox, name:'Campaign (required)', id:'campaign', required: true}}
          onChangeValue={this.setValue}/>
        <InputField
          component={{class:Textbox, name:'Amount', id:'amount'}}
          onChangeValue={this.setValue}/>
        <InputField
          component={{class:Textbox, name:'Repeat', id:'repeat'}}
          onChangeValue={this.setValue}/>
        <InputField
          component={{class:Textbox, name:'Value', id:'value', required: true}}
          onChangeValue={this.setValue}/>
        <InputField component={{class:SubmitButton}}/>
      </form>
    )
  }
}
class InputField extends React.Component {
  render () {
    let component = this.props.component
    return (
      <div className='row'>
        <div className='input-field col s12'>
          <component.class
            name={component.name}
            id={component.id}
            required={component.required}
            onChangeValue={this.props.onChangeValue}/>
        </div>
      </div>
    )
  }
}
class RadioButton extends React.Component {
  render () {
    let radio = this.props.radio
    return (
      <p>
        <input type='radio'
          id={this.props.id}
          name={this.props.name}
          value={radio.value}
          checked={radio.checked}
          onChange={this.props.checkedHandler}/>
        <label htmlFor={this.props.id}> {radio.label} </label>
      </p>
    )
  }
}
class RadioForm extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      name: this.props.name,
      radios: [
      {
        label: 'Currency',
        value: 'currency',
        checked: true
      },
      {
        label: 'Product',
        value: 'product',
        checked: false
      },
      {
        label: 'ETC.',
        value: 'ETC.',
        checked: false
      }]
    }
    this.checkedHandler = this.checkedHandler.bind(this)
  }
  checkedHandler (e) {
    this.setState({
      radios: this.state.radios.map((radio, i) => {
        radio.checked = this.state.name + i === e.target.id
        return radio
      })
    })
  }
  render () {
    return (
      <div>
      {
        this.state.radios.map((radio, i) => {
          return (
            <RadioButton
              id={this.state.name + i}
              name={this.state.name}
              key={i}
              radio={radio}
              checkedHandler={this.checkedHandler}/>
          )
        })
      }
      </div>
    )
  }
}
RadioForm.propTypes = { name: React.PropTypes.string.isRequired }
class Textbox extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      text: ''
    }
    this.handleTextChange = this.handleTextChange.bind(this)
  }
  handleTextChange (e) {
    this.props.onChangeValue(e, this.props.id)
    this.setState({
      text: e.target.value
    })
  }
  render () {
    return (
      <div>
        <input type='text'
          name={this.props.name}
          id={this.props.id}
          required={this.props.required}
          onChange={this.handleTextChange}
          value={this.state.text}/>
        <label htmlFor={this.props.id}> {this.props.name} </label>
      </div>
    )
  }
}
Textbox.propTypes = {
  id: React.PropTypes.string.isRequired,
  name: React.PropTypes.string.isRequired
}
class SubmitButton extends React.Component {
  render () {
    return (
      <button className='btn cyan waves-effect waves-light right' type='submit' value='Submit'>
        Submit
      </button>
    )
  }
}
class SerialBox extends React.Component {
  constructor (props) {
    super(props)
    this.handleSerialSubmit = this.handleSerialSubmit.bind(this)
  }
  handleSerialSubmit (data) {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      type: 'POST',
      data: data,
      success: function (data) {
        //We set the state again after submission, to update with the submitted data
        this.setState({data: data})
      }.bind(this),
      error: function (xhr, status, err) {
        console.error(this.props.url, status, err.toString())
      }.bind(this)
    })
  }
  render () {
    //we list donations, then show the form for new donations
    return (
      <div className="serialBox">
        <h1>Serial</h1>
        <SerialForm onSerialSubmit={this.handleSerialSubmit} />
      </div>
    )
  }
}
ReactDOM.render(
  <SerialBox url='/api/serials'/>,
  document.getElementById('content')
)
