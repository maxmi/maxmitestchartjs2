import React from 'react';
var BigInput = React.createClass({
  getInitialState: function() {
    return {obj:{}};
  },
  handleSubmit: function(e){
    e.preventDefault()
    console.log(this.state.obj);
  },
  setValue: function(e, key) {
    var nobj = this.state.obj;
    nobj[key] = e.target.value
    this.setState({obj:nobj})
  },
  render: function(){
    return (
      <form className='biginput' onSubmit={this.handleSubmit}>
      //checkbox
      <div>
      <CheckboxInput name="TH" id="thcheckbox" onChangeValue={this.setValue}/>
      <CheckboxInput name="JP" id="jpcheckbox" onChangeValue={this.setValue}/>
      <CheckboxInput name="TW" id="twcheckbox" onChangeValue={this.setValue}/>
      <CheckboxInput name="HK" id="hkcheckbox" onChangeValue={this.setValue}/>
      <CheckboxInput name="ETC" id="etccheckbox" onChangeValue={this.setValue}/>
      </div>
      //radio
      <DropdownInput provinces={[{name:'BKK', id:'province'},{name:'CM', id:'province'},{name:'AT', id:'province'}]} onChangeValue={this.setValue}/>
      <div className="submitbutton">
      <button className="submitButton" type="submit" value="Submit"> Submit </button>
      </div>
      </form>
    );
  }
});


var CheckboxInput = React.createClass({
  getInitialState: function() {
    return {check: false};
  },
  HandlecheckedChange: function(e) {
    e.target.value = e.target.checked
    console.log(e.target.checked)
    this.props.onChangeValue(e, this.props.id)
    this.setState({check: e.target.checked})
  },
  render: function() {
    return (
      <a>
      <input type="checkbox" checked={this.state.check} id={this.props.id} name={this.props.name} onChange={this.HandlecheckedChange} value={this.state.checked}/> {this.props.name}
      </a>
    );
  }
});
// var RadioForm = React.createClass({
//   getInitialState: function() {
//     return {choose: '',
//       handleChange : this.handleChange.bind(this)};
//   },
//   handleChange: function(e) {
//     this.setState({choose: e.target.value});
//   },
//   render: function() {
//     return (
//       <div>
//       {
//         this.props.name.map(function(name, i) {
//           return (
//             <RadioInput id={name.id} key={i} name={name.name} value={name.name} checked={check == name.name} onChange={this.state.handleChange} />
//           );
//         })
//       }
//       </div>
//     );
//   }
// });
// var RadioInput = React.createClass({
//   render: function() {
//     return (
//       <input type="radio" id={this.props.id} name={this.props.name} value={this.props.name} checked={check == this.props.name} onChange={this.props.onChange} />
//     );
//   }
// });
var DropdownInput = React.createClass({
    getInitialState: function() {
      return {provinces: this.props.provinces};
    },
    handleSubmit: function(e) {
      e.preventDefault();
      this.props.onChangeValue(e,this.state.provinces[0].id)
      this.setState({province: e.target.value})
    },
    render: function(){
      var provinces = this.props.provinces.map(function(province, i){
        return (
          <option key={i} value={province.name} id={province.id}>{province.name}</option>
        );
      });
      return (
        <div className="DropdownInput">
        <select onChange={this.handleSubmit}>
        <option value={null}>please select city</option>
        {provinces}
        </select>
        </div>
      );
    }
});

module.exports = BigInput;
