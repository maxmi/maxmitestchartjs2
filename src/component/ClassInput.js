import React from 'react';
class ClassInput extends React.Component {
  constructor () {
    super()
    this.setValue = this.setValue.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }
  handleSubmit(e){
    e.preventDefault()
    console.log("OMG")
    console.log(this.state);
    console.log("OMG")
  }
  setValue(e, key) {
    let obj = {}
    obj[key] = e.target.value
    this.setState(obj)
  }
  render() {
    console.log(this.state)
    return(
      <form className="bigInput" onSubmit={this.handleSubmit}>
      <CreateInput
        preference={{class:CheckboxInput, name: 'TH', id: 'thcheckbox'}}
        onChangeValue={this.setValue}/>
      <CreateInput
        preference={{class:CheckboxInput, name: 'JP', id: 'jpcheckbox'}}
        onChangeValue={this.setValue}/>
      <CreateInput
        preference={{class:CheckboxInput, name: 'TW', id: 'twcheckbox'}}
        onChangeValue={this.setValue}/>
      <CreateInput
        preference={{class:TextInput, name: '', id: 'text'}}
        onChangeValue={this.setValue}/>
      <CreateInput
      preference={{class:SubmitButton}}/>
      </form>
    );
  }
}

class CreateInput extends React.Component {
  render() {
        let preference = this.props.preference
    return (
      <preference.class
      name={preference.name}
      id={preference.id}
      onChangeValue={this.props.onChangeValue}/>
    );
  }
}

class CheckboxInput extends React.Component {
  constructor() {
    super()
    this.state = {
      check : false
    };
    this.handleChangeValue = this.handleChangeValue.bind(this)
  }
  handleChangeValue(e) {
    e.target.value = e.target.checked
    this.props.onChangeValue(e, this.props.id)
    this.setState({check: e.target.checked});
  }
  render() {
    return (
      <a>
      <input type="checkbox"
      name={this.props.name}
      id={this.props.id}
      value={this.state.check}
      checked={this.state.check}
      onChange={this.handleChangeValue}/>
      {this.props.name}
      </a>
    );
  }
}
class TextInput extends React.Component {
  constructor() {
    super()
    this.state= {
      text: ''
    };
    this.handleTextChange = this.handleTextChange.bind(this)
  }
  handleTextChange(e) {
    this.props.onChangeValue(e, this.props.id)
    this.setState({text: e.target.value});
  }
  render() {
    return (
      <div>
      Notification message:
      <input type="text"
      name={this.props.name}
      id={this.props.id}
      value={this.state.text}
      onChange={this.handleTextChange}/>
      </div>
    );
  }
}
class SubmitButton extends React.Component {
  render() {
    return (
      <button type="submit" value="Submit">Submit</button>
    );
  }
}
// var DropdownInput = React.createClass({
//     getInitialState: function() {
//       return {provinces: this.props.provinces};
//     },
//     handleSubmit: function(e) {
//       e.preventDefault();
//       this.props.onChangeValue(e,this.state.provinces[0].id)
//       this.setState({province: e.target.value})
//     },
//     render: function(){
//       var provinces = this.props.provinces.map(function(province, i){
//         return (
//           <option key={i} value={province.name} id={province.id}>{province.name}</option>
//         );
//       });
//       return (
//         <div className="DropdownInput">
//         <select onChange={this.handleSubmit}>
//         <option value={null}>please select city</option>
//         {provinces}
//         </select>
//         </div>
//       );
//     }
// });
//
module.exports = ClassInput;
