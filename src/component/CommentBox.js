import React from 'react';

var CommentBox = React.createClass({
  getInitialState: function() {
    return {data: this.props.data, siz: this.props.data.length};
  },
  handleCommentSubmit: function(comment){
    var comments = this.state.data;
    comment.id = Date.now();
    var newComments = comments.concat([comment]);
    this.setState({data: newComments});
  },
  handleSizeChanging: function(siz){
    console.log(siz);
    if(this.state.siz < siz){
      this.setState({siz: siz});
    }
  },
  render: function() {
  console.log(this.state.data);
    return (
      <div className="commentBox">
        <h2>Comments</h2>
       <h1>{this.state.siz}</h1>
      <CommentList data={this.state.data} onChange={this.handleSizeChanging}/>
      <CommentForm onChange={this.handleCommentSubmit}/>
      {CommentForm.props.author}
      </div>
    );
  }
});
// tutorial2.js
var CommentList = React.createClass({
  handleSize: function(e){
    console.log(e);
    this.props.onChange(e);
  },
  render: function() {
    var commentNodes = this.props.data.map(function(comment, i) {
      return (
        <Comment author={comment.author} key={i} text={comment.text}></Comment>
      );
    });
    return (
      <div className="commentList" onChange={this.handleSize(commentNodes.length)}>
      {commentNodes}
      </div>
    );
  }
});

// tutorial4.js
var Comment = React.createClass({
  render: function() {
    return (
      <div className="comment">
      <h2 className="commentAuthor">
      {this.props.author}
      </h2>
      {this.props.text}
      </div>
    );
  }
});

var CommentForm = React.createClass({
  getInitialState: function() {
    return {author: '', text: ''};
  },
  handleAuthorChange: function(e) {
    this.setState({author: e.target.value});
  },
  handleTextChange: function(e) {
    this.setState({text: e.target.value});
  },
  handleIdChange: function(e) {
    this.setState({id: e.target.value});
  },
  handleSubmit: function(e) {
    e.preventDefault();
    var author = this.state.author.trim();
    var text = this.state.text.trim();
    if (!text || !author) {
      return;
    }
    // TODO: send request to the server
    this.props.onChange({author: author, text: text});
    this.setState({author: '', text: ''});
  },
  render: function() {
    return (
      <form className="commentForm" onSubmit={this.handleSubmit}>
      <input type="text" placeholder="Your name" value={this.state.author} onChange={this.handleAuthorChange}/>
      <input type="text" placeholder="Say something..." value={this.state.text} onChange={this.handleTextChange}/>
      <input type="submit" value="Post" />
      </form>
    );
  }
});



module.exports = CommentBox;
// export default CommentBox;
